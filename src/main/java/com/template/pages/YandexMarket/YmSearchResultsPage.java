package com.template.pages.YandexMarket;

import com.template.blocks.ProductCard;
import com.template.lib.Page;
import com.template.lib.PageFactory;
import com.template.lib.annotations.ActionTitle;
import com.template.lib.annotations.ElementTitle;
import com.template.lib.annotations.PageEntry;
import org.apache.poi.poifs.storage.HeaderBlock;
import org.junit.Assert;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

import java.util.List;

/**
 * Created by sbt-svetlakov-av on 12.05.17.
 */
@PageEntry(title = "Результаты поиска товаров")
public class YmSearchResultsPage extends Page {

    private HeaderBlock headerBlock;

    @ElementTitle("Список товаров")
    @FindBy(xpath = "//div[contains(@class,'n-snippet-card')]")
    private List<ProductCard> productCards;

    public YmSearchResultsPage() {
        PageFactory.initElements(
                new HtmlElementDecorator(new HtmlElementLocatorFactory(PageFactory.getDriver())), this);
    }


    @ActionTitle("проверяет присутствие продукта")
    public void compareProductCost(String productName) {

        for (ProductCard card : productCards) {
            if (productName.toLowerCase().equals(card.getProductName().toLowerCase())) {
                return;
            }
        }

        Assert.fail("Продукт " + productName + " не был найден");
    }
}
