package com.template.pages.YandexMarket;

import com.template.blocks.HeaderBlock;
import com.template.lib.Page;
import com.template.lib.PageFactory;
import com.template.lib.annotations.ElementTitle;
import com.template.lib.annotations.PageEntry;
import com.template.lib.extensions.DriverExtension;
import gherkin.lexer.Pa;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

@PageEntry(title = "Яндекс Маркет")
public class YmMainPage extends Page {

    @ElementTitle("Меню поиска")
    public HeaderBlock headerBlock;

    public YmMainPage() {
        PageFactory.getDriver().get("http://market.yandex.ru");
        DriverExtension.waitForPageToLoad();
        PageFactory.initElements(
                new HtmlElementDecorator(new HtmlElementLocatorFactory(PageFactory.getDriver())), this);
    }
}