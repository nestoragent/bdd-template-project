Feature: initial test

  @smoke
  @initialExample
  Scenario Outline: initial test
    * User go to the page "<url>"

    Examples:
      | url        |
      | google.com |
      | ya.ru      |